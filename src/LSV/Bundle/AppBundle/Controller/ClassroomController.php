<?php

namespace LSV\Bundle\AppBundle\Controller;

use LSV\Bundle\AppBundle\Entity\Classroom;
use LSV\Bundle\AppBundle\Entity\ClassroomCouncil;
use LSV\Bundle\AppBundle\Entity\StudentCouncil;
use LSV\Bundle\AppBundle\Entity\StudentSkill;
use LSV\Bundle\AppBundle\Form\Type\ClassroomType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ClassroomController extends Controller
{
    public function indexAction($active)
    {
        $em = $this->getDoctrine()->getManager();
        $classrooms = $em->getRepository('LSVAppBundle:Classroom')
            ->findByUser($this->getUser())
        ;

        return $this->render('LSVAppBundle:Classroom:index.html.twig', array(
            'classrooms' => $classrooms,
            '_active' => $active,
        ));
    }

    public function showAction(Classroom $classroom)
    {
        $usr = $classroom->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();

        $lastExams = $em->getRepository('LSVEventBundle:Exam')->lastExams(
            $classroom->getLessonIds()
        );

        $deleteForm = $this->createDeleteForm($classroom);

        return $this->render('LSVAppBundle:Classroom:show.html.twig', array(
            'classroom' => $classroom,
            'last_exams' => $lastExams,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function createAction(Request $request)
    {
        $classroom = new Classroom();

        $form = $this->createForm(ClassroomType::class, $classroom, array(
            'validation_groups' => array('create'),
        ));
        $formType = 'create';

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $trimesters = $em->getRepository('LSVEventBundle:Trimester')->findAll();
                $skills = $em->getRepository('LSVAppBundle:Skill')->findAll();
                $students = $classroom->getStudents();
                foreach ($trimesters as $trimester) {
                    $classroom->addClassroomCouncil(ClassroomCouncil::withClassroomTrimester(
                        $classroom,
                        $trimester
                    ));

                    foreach ($students as $student) {
                        $student->addStudentCouncil(StudentCouncil::withStudentTrimester(
                            $student,
                            $trimester
                        ));
                        foreach ($skills as $skill) {
                            $student->addStudentSkill(StudentSkill::withStudentTrimester(
                                $student,
                                $trimester,
                                $skill
                            ));
                        }
                    }
                }

                $classroom->setUser($this->getUser());

                $em->persist($classroom);
                $em->flush();

                $this->addFlash(
                    'success',
                    'La classe a été enregistrée avec succès.'
                );

                return $this->redirectToRoute('classroom_show', array(
                    'id' => $classroom->getId()
                ));
            }
        }

        return $this->render('LSVAppBundle:Classroom:edit.html.twig', array(
            'form' => $form->createView(),
            'form_type' => $formType,
        ));
    }

    public function editAction(Request $request, Classroom $classroom)
    {
        $usr = $classroom->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $old_students = $classroom->getStudents();

        $formType = 'edit';
        $form = $this->createForm(ClassroomType::class, $classroom, array(
            'validation_groups' => array($formType),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $trimesters = $em->getRepository('LSVEventBundle:Trimester')->findAll();
                $classroomCouncils = $classroom->getClassroomCouncils();
                $students = $classroom->getStudents();
                foreach ($trimesters as $trimester) {
                    foreach ($students as $student) {
                        if (!$old_students->contains($student)) {
                            $student->addStudentCouncil(StudentCouncil::withStudentTrimester(
                                $student,
                                $trimester
                            ));
                            foreach ($skills as $skill) {
                                $student->addStudentSkill(StudentSkill::withStudentTrimester(
                                    $student,
                                    $trimester,
                                    $skill
                                ));
                            }
                        }
                    }
                }

                $em->persist($classroom);
                $em->flush();

                $this->addFlash(
                    'success',
                    'La classe a été modifiée avec succès.'
                );

                return $this->redirectToRoute('classroom_show', array(
                    'id' => $classroom->getId()
                ));
            }
        }

        return $this->render('LSVAppBundle:Classroom:edit.html.twig', array(
            'classroom' => $classroom,
            'form' => $form->createView(),
            'form_type' => $formType,
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $classroom = $em->getRepository('LSVAppBundle:Classroom')
            ->findOneById($id)
        ;

        if (!$classroom) {
            $this->addFlash(
                'danger',
                sprintf(
                    'La classe n° %d n\'a pas été trouvée',
                    $id
                )
            );
            return $this->redirectToRoute('event_index');
        }

        $usr = $classroom->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($classroom);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($classroom);
            $em->flush();

            $this->addFlash(
                'success',
                'La classe a été supprimée avec succès.'
            );
        }
        else {
            $this->addFlash(
                'danger',
                'La classe n\'a pas été supprimée.'
            );
        }

        return $this->redirectToRoute('event_index');
    }

    private function createDeleteForm(Classroom $classroom)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('classroom_delete', array(
                'id' => $classroom->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
