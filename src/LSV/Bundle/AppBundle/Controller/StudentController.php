<?php

namespace LSV\Bundle\AppBundle\Controller;

use LSV\Bundle\AppBundle\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StudentController extends Controller
{
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('LSVAppBundle:Student')
            ->findOneById($id)
        ;

        if (!$student) {
            throw $this->createNotFoundException(sprintf(
                'L\'étudiant %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $student->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $adj_student = $em->getRepository('LSVAppBundle:Student')
            ->getAdjacentsStudent($student)
        ;

        $deleteForm = $this->createDeleteForm($student);

        return $this->render('LSVAppBundle:Student:show.html.twig', array(
            'delete_form' => $deleteForm->createView(),
            'student' => $student,
            'adj_student' => $adj_student,
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('LSVAppBundle:Student')
            ->findOneById($id)
        ;

        if (!$student) {
            $this->addFlash(
                'danger',
                sprintf(
                    'L\'étudiant n° %d n\'a pas été trouvé',
                    $id
                )
            );
            return $this->redirectToRoute('classroom_show', array(
                'id' => $classroom->getId(),
            ));
        }

        $usr = $student->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($student);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $classroom = $student->getClassroom();

            $em->remove($student);
            $em->flush();

            $this->addFlash(
                'success',
                'L\'étudiant a été supprimé avec succès.'
            );
        }
        else {
            $this->addFlash(
                'danger',
                'L\'étudiant n\'a pas été supprimé.'
            );
        }

        return $this->redirectToRoute('classroom_show', array(
            'id' => $classroom->getId(),
        ));
    }

    private function createDeleteForm(Student $student)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('student_delete', array(
                'id' => $student->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
