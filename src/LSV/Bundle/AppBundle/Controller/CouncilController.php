<?php

namespace LSV\Bundle\AppBundle\Controller;

use LSV\Bundle\AppBundle\Entity;
use LSV\Bundle\AppBundle\Form\Type\ClassroomCouncilType;
use LSV\Bundle\AppBundle\Form\Type\StudentCouncilsType;
use LSV\Bundle\EventBundle\Entity\Trimester;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Request;

class CouncilController extends Controller
{
    public function createAction(Request $request, $type)
    {
        /* Chosen which council to create.
         * according to the $type (parameter of the URL) */
        $council = ('classroom' == $type) ? new Entity\ClassroomCouncil() : new Entity\StudentCouncil();

        $form = $this->createForm(CouncilType::class, $council, array(
            'validation_groups' => array('create'),
            'type' => $type,
        ));
        $formType = 'create';

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                // Saving in the database
                $em->persist($council);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le conseil a été enregistré avec succès.'
                );

                // If the form is well complete return to index of classrooms
                return $this->redirectToRoute('classroom_index');
            }
        }

        /* If the user does not submit the form or if the form is not valid
         * display edit view */
        return $this->render('LSVAppBundle:Council:edit.html.twig', array(
            'form' => $form->createView(),
            'form_type' => $formType,
            'type' => $type,
        ));
    }

    /**
     * @ParamConverter("trimester", options={"id" = "trimester_id"})
     */
    public function editAction(Request $request, $type, $id, Trimester $trimester)
    {
        $em = $this->getDoctrine()->getManager();
        if ($type == 'classroom') {
            $council = $em->getRepository('LSVAppBundle:ClassroomCouncil')
                ->findOneBy(array(
                    'classroom' => $id,
                    'trimester' => $trimester,
            ));
        } else {
            $council = $em->getRepository('LSVAppBundle:StudentCouncil')
                ->findOneBy(array(
                    'student' => $id,
                    'trimester' => $trimester,
            ));
        }

        /* In case of null object, inform that the council was not found
         *  and stopped the function */
        if (!$council) {
            throw $this->createNotFoundException(sprintf(
                'Le conseil n\'a pas été trouvé'
            ));
        }

        $usr = ($type == 'classroom') ? $council->getClassroom()->getUser() : $council->getStudent()->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $formType = 'edit';
        $form = $this->createForm(ClassroomCouncilType::class, $council, array(
            'validation_groups' => array($formType),
            'data_class' => ($type == 'classroom') ? Entity\ClassroomCouncil::class : Entity\StudentCouncil::class,
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $em->persist($council);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le conseil a été modifié avec succès.'
                );

                if ($type == 'classroom') {
                    return $this->redirectToRoute('classroom_show', array(
                        'id' => $council->getClassroom()->getId()
                    ));
                }
                else {
                    return $this->redirectToRoute('student_show', array(
                        'id' => $council->getStudent()->getId()
                    ));
                }

            }
        }

        return $this->render('LSVAppBundle:Council:edit.html.twig', array(
            'council' => $council,
            'form' => $form->createView(),
            'type' => $type,
            'form_type' => $formType,
        ));
    }

    /**
     * @ParamConverter("classroom", options={"id" = "classroom_id"})
     * @ParamConverter("trimester", options={"id" = "trimester_id"})
     */
    public function editStudentsAction(Request $request, Entity\Classroom $classroom, Trimester $trimester)
    {
        $usr = $classroom->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder($classroom, array(
            'validation_groups' => array('edit'),
            'translation_domain' => 'LSVAppBundle',
        ))
            ->add('students', Type\CollectionType::class, array(
                'entry_type' => StudentCouncilsType::class,
                'by_reference' => false,
                'error_bubbling' => true,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
            ->getForm()
        ;

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($classroom);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le conseil a été modifié avec succès.'
                );

                return $this->redirectToRoute('classroom_show', array(
                    'id' => $classroom->getId()
                ));
            }
        }

        return $this->render('LSVAppBundle:Council:edit_students.html.twig', array(
            'classroom' => $classroom,
            'form' => $form->createView(),
        ));
    }
}
