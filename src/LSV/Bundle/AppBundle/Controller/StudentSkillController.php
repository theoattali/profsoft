<?php

namespace LSV\Bundle\AppBundle\Controller;

use LSV\Bundle\AppBundle\Entity\Student;
use LSV\Bundle\AppBundle\Form\Type\StudentSkillType;
use LSV\Bundle\EventBundle\Entity\Trimester;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Request;

class StudentSkillController extends Controller
{
    /**
     * @ParamConverter("student", options={"id" = "student_id"})
     * @ParamConverter("trimester", options={"id" = "trimester_id"})
     */
    public function editAction(Request $request, Student $student, Trimester $trimester)
    {
        $usr = $student->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder($student, array(
            'validation_groups' => array('edit'),
            'translation_domain' => 'LSVAppBundle',
        ))
            ->add('studentSkills', Type\CollectionType::class, array(
                'entry_type' => StudentSkillType::class,
                'by_reference' => false,
                'error_bubbling' => true,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
            ->getForm()
        ;

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $em->persist($student);
                $em->flush();

                $this->addFlash(
                    'success',
                    'La compétence a été modifiée avec succès.'
                );

                return $this->redirectToRoute('student_show', array(
                    'id' => $student->getId()
                ));
            }
        }

        return $this->render('LSVAppBundle:Skill:edit.html.twig', array(
            'student' => $student,
            'form' => $form->createView(),
        ));
    }
}
