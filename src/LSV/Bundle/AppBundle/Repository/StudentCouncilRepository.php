<?php

namespace LSV\Bundle\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use LSV\Bundle\AppBundle\Entity\StudentCouncil;

class StudentCouncilRepository extends EntityRepository
{
    public function getStudentCouncils($classroom, $trimester)
    {
        $qb = $this->createQueryBuilder('sc');

        $qb->where('sc.trimester = :trimester')
           ->setParameter('trimester', $trimester)
           ->innerJoin('sc.student', 's')
           ->andWhere('s.classroom = :classroom')
           ->setParameter('classroom', $classroom);

        return $qb->getQuery()->getResult();
    }
}
