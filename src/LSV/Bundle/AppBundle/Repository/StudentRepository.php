<?php

namespace LSV\Bundle\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use LSV\Bundle\AppBundle\Entity\Student;

class StudentRepository extends EntityRepository
{
    public function getAdjacentsStudent($student)
    {
        $qb = $this->createQueryBuilder('s');

        return array(
            'next' => $this->getNextStudent($qb, $student),
            'prev' => $this->getPrevStudent($qb, $student),
        );
    }

    private function getNextStudent($qb, $student)
    {
        $qb->where('s.lastname > :student')
           ->setParameter('student', $student->getLastname())
           ->andWhere('s.classroom = :classroom')
           ->setParameter('classroom', $student->getClassroom())
           ->orderBy('s.lastname', 'ASC')
           ->addOrderBy('s.firstname', 'ASC');

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }

    private function getPrevStudent($qb, $student)
    {
        $qb->where('s.lastname < :student')
           ->setParameter('student', $student->getLastname())
           ->andWhere('s.classroom = :classroom')
           ->setParameter('classroom', $student->getClassroom())
           ->orderBy('s.lastname', 'DESC')
           ->addOrderBy('s.firstname', 'DESC');

        return $qb->getQuery()->setMaxResults(1)->getOneOrNullResult();
    }
}
