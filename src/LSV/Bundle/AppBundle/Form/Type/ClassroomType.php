<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\Classroom;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassroomType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('label', Type\TextType::class, array(
                'attr' => array(
                    'placeholder' => 'form.classroom.label',
                ),
            ))
            ->add('color', Type\HiddenType::class)
            ->add('students', Type\CollectionType::class, array(
                'label' => 'label.students',
                'entry_type' => StudentType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => false,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Classroom::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_classroom';
    }
}
