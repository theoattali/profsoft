<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('lastname', Type\TextType::class, array(
                'label' => 'form.student.lastname',
                'attr' => array(
                    'placeholder' => 'form.student.lastname'
                ),
            ))
            ->add('firstname', Type\TextType::class, array(
                'label' => 'form.student.firstname',
                'attr' => array(
                    'placeholder' => 'form.student.firstname'
                ),
            ))
            ->add('birthdate', Type\BirthdayType::class, array(
                'label' => 'form.student.birthdate.label',
                'required' => false,
                'placeholder' => array(
                    'year' => 'form.student.birthdate.year',
                    'month' => 'form.student.birthdate.month',
                    'day' => 'form.student.birthdate.day',
                ),
                'years' => range(date('Y'), date('Y') - 40),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Student::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_student';
    }
}
