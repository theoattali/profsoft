<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\Council;
use LSV\Bundle\AppBundle\Entity\StudentCouncil;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentCouncilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('council', CouncilType::class, array(
                'data_class' => Council::class,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => StudentCouncil::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_student_council';
    }
}
