<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\ClassroomCouncil;
use LSV\Bundle\AppBundle\Entity\Council;
use LSV\Bundle\AppBundle\Form\Type\CouncilType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClassroomCouncilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('council', CouncilType::class, array(
                'data_class' => Council::class,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ClassroomCouncil::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_classroom_council';
    }
}
