<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\StudentSkill;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StudentSkillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('level', Type\ChoiceType::class, array(
                'label' => 'form.skill.skill',
                'placeholder' => 'placeholder.level',
                'required' => false,
                'choices' => array(
                    'form.level.acquired' => 1,
                    'form.level.beingAcquired' => 0,
                    'form.level.nonAcquired' => -1,
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => StudentSkill::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_student_skill';
    }
}
