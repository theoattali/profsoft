<?php

namespace LSV\Bundle\AppBundle\Form\Type;

use LSV\Bundle\AppBundle\Entity\Council;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CouncilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('advice', Type\TextareaType::class, array(
                'label' => 'form.council.advice',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.council.advice',
                ),
            ))
            ->add('userAdvice', Type\TextareaType::class, array(
                'label' => 'form.council.userAdvice',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.council.userAdvice',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Council::class,
            'translation_domain' => 'LSVAppBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_council';
    }
}
