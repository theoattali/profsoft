<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\ClassroomCouncil;

class LoadClassroomCouncilData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'classroomCouncil_1' => array(
                'classroom' => 'classroom_1',
                'trimester' => 'trimester_1',
                'council' => 'council_1',
            ),
            'classroomCouncil_2' => array(
                'classroom' => 'classroom_1',
                'trimester' => 'trimester_2',
                'council' => 'council_2',
            ),
            'classroomCouncil_3' => array(
                'classroom' => 'classroom_1',
                'trimester' => 'trimester_3',
                'council' => 'council_3',
            ),
            'classroomCouncil_4' => array(
                'classroom' => 'classroom_5',
                'trimester' => 'trimester_1',
                'council' => 'council_4',
            ),
            'classroomCouncil_5' => array(
                'classroom' => 'classroom_5',
                'trimester' => 'trimester_2',
                'council' => 'council_5',
            ),
            'classroomCouncil_6' => array(
                'classroom' => 'classroom_5',
                'trimester' => 'trimester_3',
                'council' => 'council_6',
            ),
            'classroomCouncil_7' => array(
                'classroom' => 'classroom_2',
                'trimester' => 'trimester_1',
                'council' => 'council_7',
            ),
            'classroomCouncil_8' => array(
                'classroom' => 'classroom_2',
                'trimester' => 'trimester_2',
                'council' => 'council_8',
            ),
            'classroomCouncil_9' => array(
                'classroom' => 'classroom_2',
                'trimester' => 'trimester_3',
                'council' => 'council_9',
            ),
            'classroomCouncil_10' => array(
                'classroom' => 'classroom_3',
                'trimester' => 'trimester_1',
                'council' => 'council_10',
            ),
            'classroomCouncil_11' => array(
                'classroom' => 'classroom_3',
                'trimester' => 'trimester_2',
                'council' => 'council_11',
            ),
        );

        foreach ($objects as $key => $object) {
            $classroomCouncil = new ClassroomCouncil();
            if ($object['classroom']) {
                $classroomCouncil->setClassroom(
                    $this->getReference($object['classroom'])
                );
            }

            if ($object['trimester']) {
                $classroomCouncil->setTrimester(
                    $this->getReference($object['trimester'])
                );
            }

            if ($object['council']) {
                $classroomCouncil->setCouncil(
                    $this->getReference($object['council'])
                );
            }

            $manager->persist($classroomCouncil);

            $this->addReference($key, $classroomCouncil);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
