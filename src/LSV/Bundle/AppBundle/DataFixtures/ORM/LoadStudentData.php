<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\Student;

class LoadStudentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'student_1' => array(
                'lastname' => 'Attali',
                'firstname' => 'Théo',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_2' => array(
                'lastname' => 'Rondet',
                'firstname' => 'Grégoire',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_3' => array(
                'lastname' => 'Felicite',
                'firstname' => 'Lenny',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_4' => array(
                'lastname' => 'Rodier',
                'firstname' => 'Guillaume',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_5' => array(
                'lastname' => 'Viel',
                'firstname' => 'Pierre-Andreas',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_6' => array(
                'lastname' => 'Chevallier',
                'firstname' => 'Pierre-Louis',
                'birthdate' => null,
                'classroom' => 'classroom_5'
            ),
            'student_7' => array(
                'lastname' => 'Lefebvre',
                'firstname' => 'Marie',
                'birthdate' => null,
                'classroom' => 'classroom_5'
            ),
            'student_8' => array(
                'lastname' => 'Thouin',
                'firstname' => 'Julien',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_9' => array(
                'lastname' => 'Martigny',
                'firstname' => 'Antoine',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_10' => array(
                'lastname' => 'Régnier',
                'firstname' => 'Corentin',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_11' => array(
                'lastname' => 'Penhard',
                'firstname' => 'Thibaut',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_12' => array(
                'lastname' => 'Guiffan',
                'firstname' => 'Florestan',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_13' => array(
                'lastname' => 'Edeyer',
                'firstname' => 'Vincent',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_14' => array(
                'lastname' => 'Duprey',
                'firstname' => 'Charles',
                'birthdate' => new \Datetime('2001-01-01'),
                'classroom' => 'classroom_5'
            ),
            'student_15' => array(
                'lastname' => 'Nourollahi',
                'firstname' => 'Artimes',
                'birthdate' => null,
                'classroom' => 'classroom_5'
            ),
            'student_16' => array(
                'lastname' => 'De Waële',
                'firstname' => 'Maxence',
                'birthdate' => null,
                'classroom' => 'classroom_5'
            ),

            'student_17' => array(
                'lastname' => 'Ammoun',
                'firstname' => 'Clara',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_18' => array(
                'lastname' => 'Aubert',
                'firstname' => 'Elise',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_19' => array(
                'lastname' => 'Brayer',
                'firstname' => 'Luc',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_20' => array(
                'lastname' => 'Charon',
                'firstname' => 'Paul',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_21' => array(
                'lastname' => 'Schneker',
                'firstname' => 'Rémy',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_22' => array(
                'lastname' => 'Claye',
                'firstname' => 'Olivier',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_23' => array(
                'lastname' => 'Coutansais',
                'firstname' => 'Benoit',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_24' => array(
                'lastname' => 'Davies',
                'firstname' => 'Lucy',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_25' => array(
                'lastname' => 'Delmon',
                'firstname' => 'Jules',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_26' => array(
                'lastname' => 'Demarty',
                'firstname' => 'Clément ',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),

            'student_27' => array(
                'lastname' => 'Dezard',
                'firstname' => 'Louis-Arthur',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_28' => array(
                'lastname' => 'Chneker',
                'firstname' => 'Rémy',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_29' => array(
                'lastname' => 'Drecq',
                'firstname' => 'Sasha',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_30' => array(
                'lastname' => 'Feron',
                'firstname' => 'Quentin',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_31' => array(
                'lastname' => 'Fontaine',
                'firstname' => 'Victoria',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_32' => array(
                'lastname' => 'Gerbelot',
                'firstname' => 'Laura',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_33' => array(
                'lastname' => 'Gervais',
                'firstname' => 'Catherine',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_34' => array(
                'lastname' => 'Giraud',
                'firstname' => 'Mathilde',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_35' => array(
                'lastname' => 'Godié',
                'firstname' => 'Juliette',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_36' => array(
                'lastname' => 'Hainaut',
                'firstname' => 'Laura',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),

            'student_37' => array(
                'lastname' => 'Lancry',
                'firstname' => 'Thomas',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_38' => array(
                'lastname' => 'Leblanc',
                'firstname' => 'Victor',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_39' => array(
                'lastname' => 'Leguay',
                'firstname' => 'Nathan',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_40' => array(
                'lastname' => 'Luton',
                'firstname' => 'Sibylle',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_41' => array(
                'lastname' => 'Magnier',
                'firstname' => 'Ilias',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_42' => array(
                'lastname' => 'Mallié',
                'firstname' => 'Petronille',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_43' => array(
                'lastname' => 'Maurier',
                'firstname' => 'Clémentine',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
            'student_44' => array(
                'lastname' => 'Mallie',
                'firstname' => 'Petronille',
                'birthdate' => new \Datetime('2000-01-01'),
                'classroom' => 'classroom_2'
            ),
        );

        foreach ($objects as $key => $object) {
            $student = new Student();
            $student->setLastname($object['lastname']);
            $student->setFirstname($object['firstname']);
            $student->setBirthdate($object['birthdate']);

            if ($object['classroom']) {
                $student->setClassroom($this->getReference($object['classroom']));
            }

            $manager->persist($student);

            $this->addReference($key, $student);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
