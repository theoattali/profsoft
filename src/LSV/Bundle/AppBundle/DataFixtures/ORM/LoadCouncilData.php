<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\Council;

class LoadCouncilData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'council_1' => array(
                'advice' => 'Classe agréable et sérieuse. Bonne participation',
                'userAdvice' => null,
            ),
            'council_2' => array(
                'advice' => 'Ambiance et notes constante',
                'userAdvice' => null,
            ),
            'council_3' => array(
                'advice' => null,
                'userAdvice' => null,
            ),
            'council_4' => array(
                'advice' => 'Classe dissipée. Mais les notes sont correctes',
                'userAdvice' => null,
            ),
            'council_5' => array(
                'advice' => 'Amélioration de l\'assiduité et de la concentration, résultats maintenus',
                'userAdvice' => null,
            ),
            'council_6' => array(
                'advice' => 'Nette amélioration des résultats, classe motivée en fin d\'année',
                'userAdvice' => null,
            ),
            'council_7' => array(
                'advice' => 'Beaucoup d\'absents. Notes catastrophiques, aucun travail.',
                'userAdvice' => null,
            ),
            'council_8' => array(
                'advice' => 'Quelques élèves se sont repris en main, mais la majorité reste absent et/ou ne travaillent pas',
                'userAdvice' => null,
            ),
            'council_9' => array(
                'advice' => 'Avertissement',
                'userAdvice' => null,
            ),
            'council_10' => array(
                'advice' => 'Encouragements',
                'userAdvice' => null,
            ),
            'council_11' => array(
                'advice' => 'Encouragements',
                'userAdvice' => null,
            ),
            'council_12' => array(
                'advice' => 'Compliments',
                'userAdvice' => null,
            ),
            'council_13' => array(
                'advice' => 'Compliments',
                'userAdvice' => null,
            ),
            'council_14' => array(
                'advice' => 'Compliments',
                'userAdvice' => null,
            ),
            'council_15' => array(
                'advice' => 'Félicitations',
                'userAdvice' => null,
            ),
            'council_16' => array(
                'advice' => 'Félicitations',
                'userAdvice' => null,
            ),
            'council_17' => array(
                'advice' => 'Félicitations',
                'userAdvice' => null,
            ),
        );

        foreach ($objects as $key => $object) {
            $council = new Council();

            $council->setAdvice($object['advice']);
            $council->setUserAdvice($object['userAdvice']);

            $manager->persist($council);

            $this->addReference($key, $council);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
