<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\Skill;

class LoadSkillData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'skill_1' => array(
                'label' => 'Écriture',
            ),
            'skill_2' => array(
                'label' => 'Structure',
            ),
            'skill_3' => array(
                'label' => 'Réflexion',
            ),
            'skill_4' => array(
                'label' => 'Connaissance',
            ),
        );

        foreach ($objects as $key => $object) {
            $skill = new Skill();
            $skill->setLabel($object['label']);

            $manager->persist($skill);

            $this->addReference($key, $skill);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
