<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\Classroom;

class LoadClassroomData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'classroom_1' => array(
                'label' => '2nde 7',
                'color' => '#7986e8',
                'user' => 'user_1',
            ),
            'classroom_2' => array(
                'label' => '1ère S2',
                'color' => '#ec9fe4',
                'user' => 'user_1',
            ),
            'classroom_3' => array(
                'label' => '1ère ST2S',
                'color' => '#e87e6c',
                'user' => 'user_1',
            ),
            'classroom_4' => array(
                'label' => 'ECJS',
                'color' => '#eae289',
                'user' => 'user_1',
            ),
            'classroom_5' => array(
                'label' => 'BTS 2',
                'color' => '#82e86f',
                'user' => 'user_1',
            ),
            'classroom_6' => array(
                'label' => 'Première',
                'color' => '#00c0ef',
                'user' => 'user_3',
            ),
        );

        foreach ($objects as $key => $object) {
            $classroom = new Classroom();
            $classroom->setLabel($object['label']);
            $classroom->setColor($object['color']);
            $classroom->setUser($this->getReference($object['user']));

            $manager->persist($classroom);

            $this->addReference($key, $classroom);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
