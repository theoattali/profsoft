<?php

namespace LSV\Bundle\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\AppBundle\Entity\StudentCouncil;

class LoadStudentCouncilData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'studentCouncil_1' => array(
                'student' => 'student_1',
                'trimester' => 'trimester_1',
                'council' => 'council_9',
            ),
            'studentCouncil_2' => array(
                'student' => 'student_1',
                'trimester' => 'trimester_2',
                'council' => 'council_10',
            ),
            'studentCouncil_3' => array(
                'student' => 'student_1',
                'trimester' => 'trimester_3',
                'council' => 'council_11',
            ),
            'studentCouncil_4' => array(
                'student' => 'student_2',
                'trimester' => 'trimester_1',
                'council' => 'council_12',
            ),
            'studentCouncil_5' => array(
                'student' => 'student_2',
                'trimester' => 'trimester_2',
                'council' => 'council_13',
            ),
            'studentCouncil_6' => array(
                'student' => 'student_2',
                'trimester' => 'trimester_3',
                'council' => 'council_14',
            ),
            'studentCouncil_7' => array(
                'student' => 'student_3',
                'trimester' => 'trimester_1',
                'council' => 'council_15',
            ),
            'studentCouncil_8' => array(
                'student' => 'student_3',
                'trimester' => 'trimester_2',
                'council' => 'council_16',
            ),
            'studentCouncil_9' => array(
                'student' => 'student_3',
                'trimester' => 'trimester_3',
                'council' => 'council_17',
            ),
        );

        foreach ($objects as $key => $object) {
            $studentCouncil = new StudentCouncil();

             if ($object['trimester']) {
                $studentCouncil->setTrimester(
                    $this->getReference($object['trimester'])
                );
            }

            if ($object['student']) {
                $studentCouncil->setStudent(
                    $this->getReference($object['student'])
                );
            }

            if ($object['council']) {
                $studentCouncil->setCouncil(
                    $this->getReference($object['council'])
                );
            }

            $manager->persist($studentCouncil);

            $this->addReference($key, $studentCouncil);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
