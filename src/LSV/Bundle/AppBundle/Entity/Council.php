<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Council
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *     max = 255,
     *     groups = {"create", "edit"}
     * )
     */
    protected $advice = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *     max = 255,
     *     groups = {"create", "edit"}
     * )
     */
    protected $userAdvice = null;

    /**
     * @ORM\OneToOne(targetEntity="LSV\Bundle\AppBundle\Entity\ClassroomCouncil", mappedBy="council")
     */
    protected $classroomCouncil;

    /**
     * @ORM\OneToOne(targetEntity="LSV\Bundle\AppBundle\Entity\StudentCouncil", mappedBy="council")
     */
    protected $studentCouncil;


    public function getId()
    {
        return $this->id;
    }

    public function setAdvice($advice = null)
    {
        $this->advice = $advice;
    }

    public function getAdvice()
    {
        return $this->advice;
    }

    public function setUserAdvice($userAdvice = null)
    {
        $this->userAdvice = $userAdvice;
    }

    public function getUserAdvice()
    {
        return $this->userAdvice;
    }
}
