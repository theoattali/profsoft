<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\ClassroomCouncil;
use LSV\Bundle\UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\AppBundle\Repository\ClassroomRepository")
 */
class Classroom
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 50,
     *     groups = {"create", "edit"}
     * )
     */
    protected $label;

    /**
     * @ORM\Column(type="string", length=30)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 30,
     *     groups = {"create", "edit"}
     * )
     */
    protected $color;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\UserBundle\Entity\User", inversedBy="classrooms")
     */
    protected $user;

    /**
     * @ORM\OneToMany(targetEntity="Student", mappedBy="classroom", cascade={"ALL"})
     * @ORM\OrderBy({"lastname" = "ASC"})
     * @Assert\Valid
     */
    protected $students;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\ClassroomCouncil", mappedBy="classroom", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $classroomCouncils;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\EventBundle\Entity\Lesson", mappedBy="classroom", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $lessons;


    public function __construct() {
        $this->students = new ArrayCollection();
        $this->classroomCouncils = new ArrayCollection();
        $this->lessons = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function addStudent(Student $student)
    {
        $student->setClassroom($this);
        return $this->students->add($student);
    }

    public function removeStudent(Student $student)
    {
        $student->setClassroom($this);
        return $this->students->removeElement($student);
    }

    public function getStudents()
    {
        return $this->students;
    }

    public function addClassroomCouncil(ClassroomCouncil $classroomCouncil)
    {
        $classroomCouncil->setClassroom($this);
        return $this->classroomCouncils->add($classroomCouncil);
    }

    public function removeClassroomCouncil(ClassroomCouncil $classroomCouncil)
    {
        $classroomCouncil->setClassroom($this);
        return $this->classroomCouncils->removeElement($classroomCouncil);
    }

    public function getClassroomCouncils()
    {
        return $this->classroomCouncils;
    }

    public function getLessons()
    {
        return $this->lessons;
    }

    public function getNextLessons() {
        $expr = Criteria::expr();
        $criteria = Criteria::create()
            ->where($expr->gte('startDatetime', new \Datetime('today')))
            ->orderBy(array('startDatetime' => Criteria::ASC))
            ->setMaxResults(4);
        return $this->lessons->matching($criteria);
    }

    public function getLessonIds()
    {
        return $this->lessons->map(function($entity) {
            return $entity->getId();
        })->toArray();
    }
}
