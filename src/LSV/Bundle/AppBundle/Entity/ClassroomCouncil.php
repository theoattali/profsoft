<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\Classroom;
use LSV\Bundle\AppBundle\Entity\Council;
use LSV\Bundle\EventBundle\Entity\Trimester;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class ClassroomCouncil
{
    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Classroom", inversedBy="classroomCouncils")
     * @ORM\Id
     */
    private $classroom;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\EventBundle\Entity\Trimester", inversedBy="classroomCouncils")
     * @ORM\Id
     */
    private $trimester;

    /**
     * @ORM\OneToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Council", inversedBy="classroomCouncil", cascade={"ALL"})
     */
    private $council;


    public static function withClassroomTrimester(Classroom $classroom, Trimester $trimester) {
        $instance = new self();
        $instance->classroom = $classroom;
        $instance->trimester = $trimester;
        $instance->council = new Council();

        return $instance;
    }

    public function setClassroom(Classroom $classroom)
    {
        $this->classroom = $classroom;
    }

    public function getClassroom()
    {
        return $this->classroom;
    }

    public function setTrimester(Trimester $trimester)
    {
        $this->trimester = $trimester;
    }

    public function getTrimester()
    {
        return $this->trimester;
    }

    public function setCouncil(Council $council)
    {
        $this->council = $council;
    }

    public function getCouncil()
    {
        return $this->council;
    }
}
