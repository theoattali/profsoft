<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\Council;
use LSV\Bundle\AppBundle\Entity\Student;
use LSV\Bundle\EventBundle\Entity\Trimester;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\AppBundle\Repository\StudentCouncilRepository")
 */
class StudentCouncil
{
    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Student", inversedBy="studentCouncils")
     * @ORM\Id
     */
    protected $student;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\EventBundle\Entity\Trimester", inversedBy="studentCouncils")
     * @ORM\Id
     */
    protected $trimester;

    /**
     * @ORM\OneToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Council", inversedBy="studentCouncil", cascade={"ALL"})
     */
    protected $council;


    public static function withStudentTrimester(Student $student, Trimester $trimester) {
        $instance = new self();
        $instance->student = $student;
        $instance->trimester = $trimester;
        $instance->council = new Council();

        return $instance;
    }

    public function setStudent(Student $student)
    {
        $this->student = $student;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function setTrimester(Trimester $trimester)
    {
        $this->trimester = $trimester;
    }

    public function getTrimester()
    {
        return $this->trimester;
    }

    public function setCouncil(Council $council)
    {
        $this->council = $council;
    }

    public function getCouncil()
    {
        return $this->council;
    }

    public function getAverage()
    {
        $average = 0;
        $nullMark = 0;
        $i = 0;
        foreach ($this->student->getExamResults() as $result) {
            // If the result has the current student and trimester
            if ($this->getTrimester() === $result->getExam()->getTrimester())
            {
                $mark = $result->getMark();
                $coef = $result->getExam()->getCoefficient();

                // If there is a mark
                if (null !== $mark) {
                    $average += ($mark * $coef);
                }
                else { // If no mark
                    $nullMark += $coef;
                }
                $i += $coef;
            }
        }

        $i = ($i - $nullMark);

        return ($i > 0) ? round($average / $i, 2).' / 20' : 'Pas de notes';
    }
}
