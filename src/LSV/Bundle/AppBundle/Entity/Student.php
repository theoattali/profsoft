<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\StudentCouncil;
use LSV\Bundle\EventBundle\Entity\ExamResult;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\AppBundle\Repository\StudentRepository")
 */
class Student
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 100,
     *     groups = {"create", "edit"}
     * )
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 100,
     *     groups = {"create", "edit"}
     * )
     */
    protected $firstname;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $birthdate = null;

    /**
     * @ORM\ManyToOne(targetEntity="Classroom", inversedBy="students")
     */
    protected $classroom;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\EventBundle\Entity\ExamResult", mappedBy="student", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $examResults;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\StudentCouncil", mappedBy="student", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $studentCouncils;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\StudentSkill", mappedBy="student", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $studentSkills;


    public function __construct() {
        $this->examResults = new ArrayCollection();
        $this->studentCouncils = new ArrayCollection();
        $this->studentSkills = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function getLastname()
    {
        return $this->lastname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function setBirthdate(\Datetime $birthdate = null)
    {
        $this->birthdate = $birthdate;
    }

    public function getBirthdate()
    {
        return $this->birthdate;
    }

    public function getBirth()
    {
        if ($this->birthdate) {
            $dateNow = new \DateTime("now");
            $interval = $dateNow->diff($this->birthdate);
            ;
            return $this->birthdate->format('d/m/Y').' ('.$interval->format('%y ans').')';
        }
        return $this->birthdate;
    }

    public function setClassroom(Classroom $classroom)
    {
        $this->classroom = $classroom;
    }

    public function getClassroom()
    {
        return $this->classroom;
    }

    public function addExamResult(ExamResult $examResult)
    {
        $examResult->setStudent($this);
        return $this->examResults->add($examResult);
    }

    public function removeExamResult(ExamResult $examResult)
    {
        $examResult->setStudent($this);
        return $this->examResults->removeElement($examResult);
    }

    public function getExamResults()
    {
        return $this->examResults;
    }

    public function addStudentCouncil(StudentCouncil $studentCouncil)
    {
        $studentCouncil->setStudent($this);
        return $this->studentCouncils->add($studentCouncil);
    }

    public function removeStudentCouncil(StudentCouncil $studentCouncil)
    {
        $studentCouncil->setStudent($this);
        return $this->studentCouncils->removeElement($studentCouncil);
    }

    public function getStudentCouncils()
    {
        return $this->studentCouncils;
    }

    public function addStudentSkill(StudentSkill $studentSkill)
    {
        $studentSkill->setStudent($this);
        return $this->studentSkills->add($studentSkill);
    }

    public function removeStudentSkill(StudentSkill $studentSkill)
    {
        $studentSkill->setStudent($this);
        return $this->studentSkills->removeElement($studentSkill);
    }

    public function getStudentSkills()
    {
        return $this->studentSkills;
    }

    public function getUsedTrimesters()
    {
        $usedTrimester = new ArrayCollection();

        $criteria = Criteria::create()
          ->orderBy(array("label" => Criteria::DESC));

        foreach ($this->examResults as $key => $examResult) {
            $trimester = $examResult->getExam()->getTrimester();
            if (!$usedTrimester->contains($trimester)) {
                $usedTrimester->add($trimester);
            }
        }

        return $usedTrimester->matching($criteria);
    }

    public function getAverage()
    {
        $average = 0;
        $nullMark = 0;
        $i = 0;
        foreach ($this->getExamResults() as $result) {
            $mark = $result->getMark();
            $coef = $result->getExam()->getCoefficient();

            // If there is a mark
            if (null !== $mark) {
                $average += ($mark * $coef);
            }
            else { // If no mark
                $nullMark += $coef;
            }
            $i += $coef;
        }

        $i = ($i - $nullMark);

        return ($i > 0) ? round($average / $i, 2).' / 20' : 'Pas de notes';
    }
}
