<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\AppBundle\Repository\SkillRepository")
 */
class Skill
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 50,
     *     groups = {"create", "edit"}
     * )
     */
    protected $label;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\StudentSkill", mappedBy="skill", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $studentSkills;


    public function getId()
    {
        return $this->id;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getStudentSkills()
    {
        return $this->studentSkills;
    }
}
