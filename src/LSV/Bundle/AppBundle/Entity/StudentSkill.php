<?php

namespace LSV\Bundle\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\Skill;
use LSV\Bundle\AppBundle\Entity\Student;
use LSV\Bundle\EventBundle\Entity\Trimester;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\AppBundle\Repository\StudentSkillRepository")
 */
class StudentSkill
{
    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Student", inversedBy="studentSkills")
     * @ORM\Id
     */
    protected $student;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\EventBundle\Entity\Trimester", inversedBy="studentSkills")
     * @ORM\Id
     */
    private $trimester;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Skill", inversedBy="studentSkills")
     * @ORM\Id
     */
    protected $skill;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $level = null;


    public static function withStudentTrimester(Student $student, Trimester $trimester, Skill $skill) {
        $instance = new self();
        $instance->student = $student;
        $instance->trimester = $trimester;
        $instance->skill = $skill;

        return $instance;
    }

    public function setStudent(Student $student)
    {
        $this->student = $student;
    }

    public function getStudent()
    {
        return $this->student;
    }

    public function setTrimester(Trimester $trimester)
    {
        $this->trimester = $trimester;
    }

    public function getTrimester()
    {
        return $this->trimester;
    }

    public function setSkill(Skill $skill)
    {
        $this->skill = $skill;
    }

    public function getSkill()
    {
        return $this->skill;
    }

    public function setLevel($level = null)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        if ($this->level === 1) {
            return 'Acquis';
        }
        else if ($this->level === 0) {
            return 'En cours d\'acquisition';
        }
        else if ($this->level === -1) {
            return 'Non acquis';
        }

    }
}
