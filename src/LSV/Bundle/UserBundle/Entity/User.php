<?php

namespace LSV\Bundle\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Validator\Constraints as Assert;
use LSV\Bundle\AppBundle\Entity\Classroom;
/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\Classroom", mappedBy="user", cascade={"ALL"})
     * @ORM\OrderBy({"label" = "ASC"})
     * @Assert\Valid
     */
    protected $classrooms;


    public function __construct()
    {
        parent::__construct();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getClassrooms()
    {
        return $this->classrooms;
    }
}

