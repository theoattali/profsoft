<?php

namespace LSV\Bundle\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\UserBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'user_1' => array(
                'username' => 'user',
                'email' => 'user@mail.com',
                'enabled' => true,
                'password' => 'user',
                'role' => array('ROLE_USER'),
            ),
            'user_2' => array(
                'username' => 'admin',
                'email' => 'admin@mail.com',
                'enabled' => true,
                'password' => 'admin',
                'role' => array('ROLE_ADMIN'),
            ),
            'user_3' => array(
                'username' => 'test',
                'email' => 'test@mail.com',
                'enabled' => true,
                'password' => 'test',
                'role' => array('ROLE_USER'),
            ),
        );

        foreach ($objects as $key => $object) {
            $user = new User();

            $user->setUsername($object['username']);
            $user->setEmail($object['email']);
            $user->setEnabled($object['enabled']);
            $user->setPlainPassword($object['password']);
            $user->setRoles($object['role']);

            $manager->persist($user);

            $this->addReference($key, $user);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
