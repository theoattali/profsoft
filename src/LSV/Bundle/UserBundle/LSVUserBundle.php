<?php

namespace LSV\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LSVUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
