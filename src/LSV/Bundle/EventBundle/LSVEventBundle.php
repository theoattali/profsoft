<?php

namespace LSV\Bundle\EventBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class LSVEventBundle extends Bundle
{
    public function getParent()
    {
        return 'ADesignsCalendarBundle';
    }
}
