<?php

namespace LSV\Bundle\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\EventBundle\Entity\ExamResult;

class LoadExamResultData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'examResult_1' => array(
                'mark' => 0,
                'comment' => 'Copie blanche',
                'exam' => 'exam_1',
                'student' => 'student_1',
            ),
            'examResult_2' => array(
                'mark' => 12,
                'comment' => 'Bien',
                'exam' => 'exam_1',
                'student' => 'student_2',
            ),
            'examResult_3' => array(
                'mark' => 20,
                'comment' => 'Excellent',
                'exam' => 'exam_1',
                'student' => 'student_3',
            ),
            'examResult_4' => array(
                'mark' => 15,
                'comment' => 'Très bien',
                'exam' => 'exam_1',
                'student' => 'student_4',
            ),
            'examResult_5' => array(
                'mark' => 10,
                'comment' => 'Moyen',
                'exam' => 'exam_1',
                'student' => 'student_5',
            ),
            'examResult_6' => array(
                'mark' => 13,
                'comment' => 'Bien',
                'exam' => 'exam_1',
                'student' => 'student_6',
            ),
            'examResult_7' => array(
                'mark' => 5,
                'comment' => 'Insuffisant',
                'exam' => 'exam_2',
                'student' => 'student_1',
            ),
            'examResult_8' => array(
                'mark' => 13,
                'comment' => 'Bien',
                'exam' => 'exam_2',
                'student' => 'student_2',
            ),
            'examResult_9' => array(
                'mark' => 19,
                'comment' => 'Excellent',
                'exam' => 'exam_2',
                'student' => 'student_3',
            ),
            'examResult_10' => array(
                'mark' => 14,
                'comment' => 'Très bien',
                'exam' => 'exam_2',
                'student' => 'student_4',
            ),
            'examResult_11' => array(
                'mark' => 11,
                'comment' => 'Moyen',
                'exam' => 'exam_2',
                'student' => 'student_5',
            ),
        );

        foreach ($objects as $key => $object) {
            $examResult = new ExamResult();

            $examResult->setMark($object['mark']);
            $examResult->setComment($object['comment']);

            if ($object['exam']) {
                $examResult->setExam($this->getReference($object['exam']));
            }

            if ($object['student']) {
                $examResult->setStudent($this->getReference($object['student']));
            }

            $manager->persist($examResult);

            $this->addReference($key, $examResult);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 5;
    }
}
