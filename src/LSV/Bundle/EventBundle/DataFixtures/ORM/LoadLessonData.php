<?php

namespace LSV\Bundle\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\EventBundle\Entity\Lesson;

class LoadLessonData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getThisDayOfWeek($str_day)
    {
        return date('Y-m-d', strtotime($str_day." this week"));;
    }

    public function load(ObjectManager $manager)
    {
        $objects = array(
            'lesson_1' => array(
                'title' => 'Expressions',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Monday").'08:40:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Monday").'09:35:00'),
                'classroom' => 'classroom_5',
            ),
            'lesson_2' => array(
                'title' => 'ECJS',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Tuesday").'9:35:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Tuesday").'10:30:00'),
                'classroom' => 'classroom_5',
            ),
            'lesson_3' => array(
                'title' => 'ECJS',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Wednesday").'9:35:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Wednesday").'10:30:00'),
                'classroom' => 'classroom_5',
            ),
            'lesson_4' => array(
                'title' => 'ECJS',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Thursday").'9:35:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Thursday").'10:30:00'),
                'classroom' => 'classroom_5',
            ),
            'lesson_5' => array(
                'title' => 'ECJS',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Friday").'9:35:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Friday").'10:30:00'),
                'classroom' => 'classroom_5',
            ),
            'lesson_6' => array(
                'title' => 'Expressions',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Wednesday").'10:45:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Wednesday").'12:35:00'),
                'classroom' => 'classroom_2',
            ),
            'lesson_7' => array(
                'title' => 'Expressions',
                'startDatetime' => new \DateTime($this->getThisDayOfWeek("Thursday").'13:45:00'),
                'endDatetime' => new \DateTime($this->getThisDayOfWeek("Thursday").'15:35:00'),
                'classroom' => 'classroom_2',
            ),
        );

        foreach ($objects as $key => $object) {
            $lesson = new Lesson();

            $lesson->setTitle($object['title']);
            $lesson->setStartDatetime($object['startDatetime']);
            $lesson->setEndDatetime($object['endDatetime']);

            if ($object['classroom']) {
                $lesson->setClassroom($this->getReference($object['classroom']));
            }

            $manager->persist($lesson);

            $this->addReference($key, $lesson);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
