<?php

namespace LSV\Bundle\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\EventBundle\Entity\Trimester;

class LoadTrimesterData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'trimester_1' => array(
                'label' => 'Trimestre 1',
                'shortLabel' => 'T1',
            ),
            'trimester_2' => array(
                'label' => 'Trimestre 2',
                'shortLabel' => 'T2',
            ),
            'trimester_3' => array(
                'label' => 'Trimestre 3',
                'shortLabel' => 'T3',
            ),
        );

        foreach ($objects as $key => $object) {
            $trimester = new Trimester();

            $trimester->setLabel($object['label']);
            $trimester->setShortLabel($object['shortLabel']);

            $manager->persist($trimester);

            $this->addReference($key, $trimester);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}
