<?php

namespace LSV\Bundle\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use LSV\Bundle\EventBundle\Entity\Exam;

class LoadExamData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $objects = array(
            'exam_1' => array(
                'title' => 'Expressions',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_1',
            ),
            'exam_2' => array(
                'title' => 'Connaissances',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_2',
            ),
            'exam_3' => array(
                'title' => 'Expressions',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_1',
            ),
            'exam_4' => array(
                'title' => 'Connaissances',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_2',
            ),
            'exam_5' => array(
                'title' => 'Expressions',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_1',
            ),
            'exam_6' => array(
                'title' => 'Connaissances',
                'coefficient' => 1,
                'trimester' => 'trimester_1',
                'lesson' => 'lesson_2',
            ),
        );

        foreach ($objects as $key => $object) {
            $exam = new Exam();

            $exam->setTitle($object['title']);
            $exam->setCoefficient($object['coefficient']);

            if ($object['trimester']) {
                $exam->setTrimester($this->getReference($object['trimester']));
            }

            if ($object['lesson']) {
                $exam->setLesson($this->getReference($object['lesson']));
            }

            $manager->persist($exam);

            $this->addReference($key, $exam);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 4;
    }
}
