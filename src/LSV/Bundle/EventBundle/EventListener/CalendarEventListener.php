<?php

namespace LSV\Bundle\EventBundle\EventListener;

use ADesigns\CalendarBundle\Entity\EventEntity;
use ADesigns\CalendarBundle\Event\CalendarEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CalendarEventListener
{
    private $entityManager;

    private $router;

    private $tokenStorage;

    public function __construct(EntityManager $entityManager, Router $router, TokenStorage $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
    }

    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function loadEvents(CalendarEvent $calendarEvent)
    {
        $startDate = $calendarEvent->getStartDatetime();
        $endDate = $calendarEvent->getEndDatetime();
        $user = $this->getUser();

        // load events using your custom logic here,
        // for instance, retrieving events from a repository

        $events = $this->entityManager->getRepository('LSVEventBundle:Lesson')
            ->createQueryBuilder('e')
            ->innerJoin('e.classroom', 'c')
            ->where('c.user = :user')
            ->setParameter('user', $user)
            ->andWhere('e.startDatetime BETWEEN :startDate and :endDate')
            ->setParameter('startDate', $startDate->format('Y-m-d H:i:s'))
            ->setParameter('endDate', $endDate->format('Y-m-d H:i:s'))
            ->getQuery()->getResult();

        foreach($events as $event) {

            // create an event with a start/end time, or an all day event
            $eventEntity = new EventEntity(
                $event->getTitle(),
                $event->getStartDatetime(),
                $event->getEndDatetime()
            );

            $eventEntity->setAllDay(false);
            $eventEntity->setTitle($event->getClassroom()->getLabel());
            $eventEntity->setBgColor($event->getBgColor());

            $eventEntity->setUrl(
                $this->router->generate('lesson_show', array(
                    'id' => $event->getId(),
                ))
            );
            if (!$event->getExams()->isEmpty()) {
                $eventEntity->addField('borderColor', '#FF0000');
            }

            // finally, add the event to the CalendarEvent for displaying on the calendar
            $calendarEvent->addEvent($eventEntity);
        }
    }
}
