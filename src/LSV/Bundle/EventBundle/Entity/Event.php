<?php

namespace LSV\Bundle\EventBundle\Entity;

use ADesigns\CalendarBundle\Entity\EventEntity as BaseEvent;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"lesson" = "Lesson"})
 */
abstract class Event extends BaseEvent
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 255,
     *     groups = {"create", "edit"}
     * )
     */
    protected $title;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $startDatetime;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $endDatetime;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    protected $otherFields;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     * @Assert\Length(
     *     max = 500,
     *     groups = {"create", "edit"}
     * )
     */
    protected $description;


    public function __construct()
    {
        $this->allDay = false;
    }

    public function setDescription($description = null)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }
}

