<?php

namespace LSV\Bundle\EventBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\Classroom;

/**
 * @ORM\Entity
 */
class Lesson extends Event
{
    protected $recurrenceTo;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Classroom", inversedBy="lessons")
     */
    protected $classroom;

    /**
     * @ORM\OneToMany(targetEntity="Exam", mappedBy="lesson", cascade={"ALL"})
     */
    protected $exams;


    public function __construct() {
        $this->exams = new ArrayCollection();
    }

    /* Override function from event entity */
    public function getBgColor()
    {
        return $this->classroom->getColor();
    }

    public function setRecurrenceTo($recurrenceTo)
    {
        $this->recurrenceTo = $recurrenceTo;
    }

    public function getRecurrenceTo()
    {
        return $this->recurrenceTo;
    }

    public function setClassroom(Classroom $classroom)
    {
        $this->classroom = $classroom;
    }

    public function getClassroom()
    {
        return $this->classroom;
    }

    public function addExam(Exam $exam)
    {
        $exam->setLesson($this);
        return $this->exams->add($exam);
    }

    public function removeExam(Exam $exam)
    {
        $exam->setLesson($this);
        return $this->exams->removeElement($exam);
    }

    public function getExams()
    {
        return $this->exams;
    }

    public function getType()
    {
        return 'lesson';
    }
}
