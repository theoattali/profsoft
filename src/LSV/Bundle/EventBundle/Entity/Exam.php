<?php

namespace LSV\Bundle\EventBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\EventBundle\Repository\ExamRepository")
 */
class Exam
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     * @Assert\Length(
     *     max = 255,
     *     groups = {"create", "edit"}
     * )
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     * @Assert\Length(
     *     max = 500,
     *     groups = {"create", "edit"}
     * )
     */
    protected $description;

    /**
     * @ORM\Column(type="float", length=255)
     * @Assert\NotBlank(
     *     groups = {"create", "edit"}
     * )
     */
    protected $coefficient;

    /**
     * @ORM\ManyToOne(targetEntity="Trimester", inversedBy="exams")
     */
    protected $trimester;

    /**
     * @ORM\ManyToOne(targetEntity="Lesson", inversedBy="exams")
     */
    protected $lesson;

    /**
     * @ORM\OneToMany(targetEntity="ExamResult", mappedBy="exam", cascade={"ALL"})
     * @Assert\Valid
     */
    protected $examResults;


    public function __construct() {
        $this->examResults = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setCoefficient($coefficient)
    {
        $this->coefficient = $coefficient;
    }

    public function getCoefficient()
    {
        return $this->coefficient;
    }

    public function setTrimester(Trimester $trimester)
    {
        $this->trimester = $trimester;
    }

    public function getTrimester()
    {
        return $this->trimester;
    }

    public function setLesson(Lesson $lesson)
    {
        $this->lesson = $lesson;
    }

    public function getLesson()
    {
        return $this->lesson;
    }

    public function addExamResult(ExamResult $examResult)
    {
        $examResult->setExam($this);
        return $this->examResults->add($examResult);
    }

    public function removeExamResult(ExamResult $examResult)
    {
        $examResult->setExam($this);
        return $this->examResults->removeElement($examResult);
    }

    public function getExamResults()
    {
        return $this->examResults;
    }

    public function getAverage()
    {
        $average = 0;
        $nullMark = 0;
        $i = 0;
        foreach ($this->getExamResults() as $result) {
            $mark = $result->getMark();
            $coef = $result->getExam()->getCoefficient();

            // If there is a mark
            if (null !== $mark) {
                $average += ($mark * $coef);
            }
            else { // If no mark
                $nullMark += $coef;
            }
            $i += $coef;
        }

        $i = ($i - $nullMark);

        return ($i > 0) ? round($average / $i, 2).' / 20' : 'Pas de notes';
    }
}

