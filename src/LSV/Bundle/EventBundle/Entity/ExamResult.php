<?php

namespace LSV\Bundle\EventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use LSV\Bundle\AppBundle\Entity\Student;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\EventBundle\Repository\ExamResultRepository")
 */
class ExamResult
{
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $mark;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(
     *     max = 255,
     *     groups = {"create", "edit"}
     * )
     */
    protected $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Exam", inversedBy="examResults")
     * @ORM\Id
     */
    protected $exam;

    /**
     * @ORM\ManyToOne(targetEntity="LSV\Bundle\AppBundle\Entity\Student", inversedBy="examResults")
     * @ORM\Id
     */
    protected $student;


    public function setMark($mark = null)
    {
        $this->mark = $mark;
    }

    public function getMark()
    {
        return $this->mark;
    }

    public function setComment($comment = null)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function setExam(Exam $exam)
    {
        $this->exam = $exam;
    }

    public function getExam()
    {
        return $this->exam;
    }

    public function setStudent(Student $student)
    {
        $this->student = $student;
    }

    public function getStudent()
    {
        return $this->student;
    }
}
