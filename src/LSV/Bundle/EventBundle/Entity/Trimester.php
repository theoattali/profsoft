<?php

namespace LSV\Bundle\EventBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="LSV\Bundle\EventBundle\Repository\TrimesterRepository")
 */
class Trimester
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $shortLabel;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\ClassroomCouncil", mappedBy="trimester", cascade={"ALL"})
     */
    protected $classroomCouncils;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\StudentCouncil", mappedBy="trimester", cascade={"ALL"})
     */
    protected $studentCouncils;

    /**
     * @ORM\OneToMany(targetEntity="LSV\Bundle\AppBundle\Entity\StudentSkill", mappedBy="trimester", cascade={"ALL"})
     */
    protected $studentSkills;

    /**
     * @ORM\OneToMany(targetEntity="Exam", mappedBy="trimester", cascade={"ALL"})
     */
    protected $exams;


    public function __construct() {
        $this->classroomCouncils = new ArrayCollection();
        $this->studentCouncils = new ArrayCollection();
        $this->studentSkills = new ArrayCollection();
        $this->exams = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setShortLabel($shortLabel)
    {
        $this->shortLabel = $shortLabel;
    }

    public function getShortLabel()
    {
        return $this->shortLabel;
    }

    public function getClassroomCouncils()
    {
        return $this->classroomCouncils;
    }

    public function getStudentSkills()
    {
        return $this->studentSkills;
    }

    public function setStudentCouncils($studentCouncils)
    {
        $this->studentCouncils = $studentCouncils;
    }

    public function getStudentCouncils()
    {
        return $this->studentCouncils;
    }

    public function getExams()
    {
        return $this->exams;
    }

    public function trimesterCheck($student)
    {
        foreach ($this->exams as $key => $exam) {
            if ($exam->getLesson()->getClassroom() == $student->getClassroom()) {
                return true;
            }
        }
        foreach ($this->studentCouncils as $key => $studentCouncil) {
            if ($exam->getStudent() == $student) {
                return true;
            }
        }
        return false;
    }
}

