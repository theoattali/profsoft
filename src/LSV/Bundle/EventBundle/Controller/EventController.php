<?php

namespace LSV\Bundle\EventBundle\Controller;

use ADesigns\CalendarBundle\Controller\CalendarController as BaseController;
use ADesigns\CalendarBundle\Event\CalendarEvent;
use Symfony\Component\HttpFoundation\Request;

class EventController extends BaseController
{
    public function loadCalendarAction(Request $request)
    {
        $startDatetime = new \DateTime();
        $startDatetime->setTimestamp(strtotime($request->get('start')));

        $endDatetime = new \DateTime();
        $endDatetime->setTimestamp(strtotime($request->get('end')));

        $events = $this->container->get('event_dispatcher')->dispatch(
            CalendarEvent::CONFIGURE,
            new CalendarEvent($startDatetime, $endDatetime, $request))->getEvents()
        ;

        $response = new \Symfony\Component\HttpFoundation\Response();
        $response->headers->set('Content-Type', 'application/json');

        $return_events = array();

        foreach($events as $event) {
            $return_events[] = $event->toArray();
        }

        $response->setContent(json_encode($return_events));

        return $response;
    }

    public function indexAction()
    {
        return $this->render('LSVEventBundle:Event:index.html.twig');
    }
}
