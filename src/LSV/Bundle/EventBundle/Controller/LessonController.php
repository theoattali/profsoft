<?php

namespace LSV\Bundle\EventBundle\Controller;

use LSV\Bundle\EventBundle\Entity;
use LSV\Bundle\EventBundle\Form\Type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LessonController extends Controller
{
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $lesson = $em->getRepository('LSVEventBundle:Lesson')->findOneById($id);

        if (!$lesson) {
            throw $this->createNotFoundException(sprintf(
                'Le cours n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $lesson->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $deleteForm = $this->createDeleteForm($lesson);

        return $this->render('LSVEventBundle:Lesson:show.html.twig', array(
            'delete_form' => $deleteForm->createView(),
            'lesson' => $lesson,
        ));
    }

    public function createAction(Request $request)
    {
        $lesson = new Entity\Lesson();
        $lesson->setAllDay('false');

        $form = $this->createForm(Type\LessonType::class, $lesson, array(
            'validation_groups' => array('create'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $recurrenceTo = $lesson->getRecurrenceTo();

                $msg = ($recurrenceTo) ? 'Les cours ont été enregistrés avec succès.' : 'Le cours a été enregistré avec succès.';

                $period = new \DatePeriod(
                    $lesson->getStartDatetime(),
                    new \DateInterval("P1W"),
                    $recurrenceTo
                );

                foreach ($period as $dt) {
                    $lessonR = clone $lesson;
                    $iterDt = clone $dt;
                    $lessonR->setStartDatetime($dt);
                    $lessonR->setEndDatetime($iterDt->modify('+55 minutes'));
                    $em->persist($lessonR);
                }
                $em->flush();

                $this->addFlash(
                    'success',
                    $msg
                );

                return $this->redirectToRoute('event_index');
            }
        }

        return $this->render('LSVEventBundle:Lesson:edit.html.twig', array(
            'form' => $form->createView(),
            'form_type' => 'create',
        ));
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $lesson = $em->getRepository('LSVEventBundle:Lesson')->findOneById($id);

        if (!$lesson) {
            throw $this->createNotFoundException(sprintf(
                'Le cours n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $lesson->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(Type\LessonType::class, $lesson, array(
            'validation_groups' => array('edit'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $lessonData = $form->getData();
                $start = $lessonData->getStartDatetime();
                $lesson->setEndDatetime($start->modify('+55 minutes'));

                $em->persist($lesson);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le cours a été modifié avec succès.'
                );

                return $this->redirectToRoute('lesson_show', array(
                    'id' => $lesson->getId()
                ));
            }
        }

        return $this->render('LSVEventBundle:Lesson:edit.html.twig', array(
            'lesson' => $lesson,
            'form' => $form->createView(),
            'form_type' => 'edit',
        ));
    }

    public function examAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $lesson = $em->getRepository('LSVEventBundle:Lesson')->findOneById($id);

        if (!$lesson) {
            throw $this->createNotFoundException(sprintf(
                'Le cours n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $lesson->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $exam = new Entity\Exam();

        $form = $this->createForm(Type\ExamType::class, $exam, array(
            'validation_groups' => array('edit'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $lesson->addExam($exam);

                $students = $exam->getLesson()->getClassroom()->getStudents();
                foreach ($students as $key => $student) {
                    $examResult = new Entity\ExamResult();
                    $examResult->setExam($exam);
                    $examResult->setStudent($student);
                    $exam->addExamResult($examResult);
                }

                $em->persist($lesson);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le contrôle a été ajouté avec succès.'
                );

                return $this->redirectToRoute('exam_show', array(
                    'id' => $exam->getId()
                ));
            }
        }

        return $this->render('LSVEventBundle:Exam:edit.html.twig', array(
            'form' => $form->createView(),
            'form_type' => 'create',
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $lesson = $em->getRepository('LSVEventBundle:Lesson')->findOneById($id);

        if (!$lesson) {
            $this->addFlash(
                'danger',
                sprintf(
                    'Le cours n° %d n\'a pas été trouvé',
                    $id
                )
            );
            return $this->redirectToRoute('event_index');
        }

        $usr = $lesson->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($lesson);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($lesson);
            $em->flush();

            $this->addFlash(
                'success',
                'Le cours a été supprimé avec succès.'
            );
        }
        else {
            $this->addFlash(
                'danger',
                'Le cours n\'a pas été supprimé.'
            );
        }

        return $this->redirectToRoute('event_index');
    }

    private function createDeleteForm($lesson)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lesson_delete', array(
                'id' => $lesson->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
