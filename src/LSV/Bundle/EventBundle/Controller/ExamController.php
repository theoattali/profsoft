<?php

namespace LSV\Bundle\EventBundle\Controller;

use LSV\Bundle\EventBundle\Entity\Exam;
use LSV\Bundle\EventBundle\Entity\ExamResult;
use LSV\Bundle\EventBundle\Form\Type\ExamResultType;
use LSV\Bundle\EventBundle\Form\Type\ExamType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\HttpFoundation\Request;

class ExamController extends Controller
{
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $exam = $em->getRepository('LSVEventBundle:Exam')->findOneById($id);

        if (!$exam) {
            throw $this->createNotFoundException(sprintf(
                'Le contrôle n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $exam->getLesson()->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $deleteForm = $this->createDeleteForm($exam);

        return $this->render('LSVEventBundle:Exam:show.html.twig', array(
            'delete_form' => $deleteForm->createView(),
            'exam' => $exam,
        ));
    }

    public function createAction(Request $request)
    {
        $exam = new Exam();

        $form = $this->createForm(ExamType::class, $exam, array(
            'validation_groups' => array('create'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                $students = $exam->getLesson()->getClassroom()->getStudents();
                foreach ($students as $key => $student) {
                    $examResult = new ExamResult();
                    $examResult->setExam($exam);
                    $examResult->setStudent($student);
                    $exam->addExamResult($examResult);
                }

                $em->persist($exam);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le contrôle '.$exam->getId().' a été enregistré avec succès.'
                );

                return $this->redirectToRoute('exam_show', array(
                    'id' => $exam->getId(),
                ));
            }
        }

        return $this->render('LSVEventBundle:Exam:edit.html.twig', array(
            'form' => $form->createView(),
            'form_type' => 'create',
        ));
    }

    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $exam = $em->getRepository('LSVEventBundle:Exam')->findOneById($id);

        if (!$exam) {
            throw $this->createNotFoundException(sprintf(
                'Le contrôle n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $exam->getLesson()->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createForm(ExamType::class, $exam, array(
            'validation_groups' => array('edit'),
        ));

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $em->persist($exam);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Le contrôle a été modifié avec succès.'
                );

                return $this->redirectToRoute('exam_show', array(
                    'id' => $exam->getId()
                ));
            }
        }

        return $this->render('LSVEventBundle:Exam:edit.html.twig', array(
            'exam' => $exam,
            'form' => $form->createView(),
            'form_type' => 'edit',
        ));
    }

    public function resultsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $exam = $em->getRepository('LSVEventBundle:Exam')
                    ->findOneById($id);
        $examResults = $em->getRepository('LSVEventBundle:ExamResult')
                    ->getExamResultsOrderByStudent($exam);

        if (!$exam) {
            throw $this->createNotFoundException(sprintf(
                'Le contrôle n° %d n\'a pas été trouvé',
                $id
            ));
        }

        $usr = $exam->getLesson()->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createFormBuilder($exam, array(
            'validation_groups' => array('edit'),
            'translation_domain' => 'LSVEventBundle',
        ))
            ->add('examResults', Type\CollectionType::class, array(
                'entry_type' => ExamResultType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'error_bubbling' => true,
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
            ->getForm()
        ;

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $em->persist($exam);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Les notes ont été modifiées avec succès.'
                );

                return $this->redirectToRoute('exam_show', array(
                    'id' => $exam->getId()
                ));
            }
        }

        return $this->render('LSVEventBundle:Exam:results.html.twig', array(
            'exam' => $exam,
            'examResults' => $examResults,
            'form' => $form->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $exam = $em->getRepository('LSVEventBundle:Exam')->findOneById($id);

        if (!$exam) {
            $this->addFlash(
                'danger',
                sprintf(
                    'Le contrôle n° %d n\'a pas été trouvé',
                    $id
                )
            );
            return $this->redirectToRoute('event_index');
        }

        $usr = $exam->getLesson()->getClassroom()->getUser();
        if ($this->getUser() !== $usr) {
            throw $this->createAccessDeniedException();
        }

        $form = $this->createDeleteForm($exam);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($exam);
            $em->flush();

            $this->addFlash(
                'success',
                'Le contrôle a été supprimé avec succès.'
            );
        }
        else {
            $this->addFlash(
                'danger',
                'Le contrôle n\'a pas été supprimé.'
            );
        }

        return $this->redirectToRoute('event_index');
    }

    private function createDeleteForm($exam)
    {

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('exam_delete', array(
                'id' => $exam->getId()
            )))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
