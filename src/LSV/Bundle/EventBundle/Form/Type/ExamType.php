<?php

namespace LSV\Bundle\EventBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, array(
                'label' => 'form.title',
                'attr' => array(
                    'placeholder' => 'form.title',
                ),
            ))
            ->add('description', Type\TextareaType::class, array(
                'label' => 'form.description',
                'attr' => array(
                    'placeholder' => 'form.description',
                ),
                'required' => false,
            ))
            ->add('coefficient', Type\NumberType::class, array(
                'label' => 'form.coefficient',
                'scale' => 2,
                'attr' => array(
                    'placeholder' => 'form.coefficient',
                ),
            ))
            ->add('trimester', EntityType::class, array(
                'label' => 'form.trimester',
                'placeholder' => 'placeholder.trimester',
                'choice_label' => 'label',
                'class' => 'LSVEventBundle:Trimester',
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LSV\Bundle\EventBundle\Entity\Exam',
            'translation_domain' => 'LSVEventBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'event_exam';
    }
}
