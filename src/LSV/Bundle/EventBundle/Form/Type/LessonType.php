<?php

namespace LSV\Bundle\EventBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class LessonType extends AbstractType
{
    private $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', Type\TextType::class, array(
                'label' => 'form.title',
                'attr' => array(
                    'placeholder' => 'form.title'
                ),
            ))
            ->add('startDatetime', Type\DateTimeType::class, array(
                'label' => 'form.startAt',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy HH:mm',
                'attr' => array(
                    'placeholder' => 'form.startAt'
                ),
            ))
            ->add('recurrenceTo', Type\DateType::class, array(
                'label' => 'form.repeatTo',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.repeatTo'
                ),
            ))
            ->add('description', Type\TextareaType::class, array(
                'label' => 'form.description',
                'required' => false,
                'attr' => array(
                    'placeholder' => 'form.description'
                ),
            ))
            ->add('classroom', EntityType::class, array(
                'label' => 'form.classroom',
                'placeholder' => 'placeholder.classroom',
                'choice_label' => 'label',
                'class' => 'LSVAppBundle:Classroom',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('c')
                              ->where('c.user = :user')
                              ->setParameter('user', $this->getUser());
                  },
            ))
            ->add('submit', Type\SubmitType::class, array(
                'label' => 'form.save',
                'attr' => array(
                    'class' => 'btn btn-success',
                ),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LSV\Bundle\EventBundle\Entity\Lesson',
            'translation_domain' => 'LSVEventBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'event_lesson';
    }
}
