<?php

namespace LSV\Bundle\EventBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExamResultType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mark', Type\IntegerType::class, array(
                'label' => 'form.mark',
                'attr' => array(
                    'placeholder' => 'form.mark'
                ),
                'required' => false,
            ))
            ->add('comment', Type\TextType::class, array(
                'label' => 'form.comment',
                'attr' => array(
                    'placeholder' => 'form.comment'
                ),
                'required' => false,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'LSV\Bundle\EventBundle\Entity\ExamResult',
            'translation_domain' => 'LSVEventBundle',
        ));
    }

    public function getBlockPrefix()
    {
        return 'event_result';
    }
}
