<?php

namespace LSV\Bundle\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use LSV\Bundle\EventBundle\Entity\Exam;

class ExamRepository extends EntityRepository
{
    public function lastExams($lesson_ids)
    {
        $qb = $this->createQueryBuilder('e');

        $qb->where("e.lesson IN (:lesson_ids)")
           ->setParameter('lesson_ids', $lesson_ids)
           ->innerJoin('e.lesson', 'l')
           ->andWhere('l.startDatetime <= :today')
           ->setParameter('today', new \Datetime('today'))
           ->orderby('l.startDatetime', 'ASC');

        return $qb->getQuery()->setMaxResults(4)->getResult();
    }
}
