<?php

namespace LSV\Bundle\EventBundle\Repository;

use Doctrine\ORM\EntityRepository;
use LSV\Bundle\EventBundle\Entity\Exam;

class ExamResultRepository extends EntityRepository
{
    public function getExamResultsOrderByStudent(Exam $exam)
    {
        $qb = $this->createQueryBuilder('er');

        $qb->innerJoin('er.student', 's')
           ->where('er.exam = :exam')
           ->setParameter('exam', $exam);

        return $qb->orderBy('s.lastname', 'DESC')->getQuery()->getResult();
    }
}
